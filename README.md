VNC Server Install
==================

This role have been setup to automate the install of VNC server and its configuration on the CentOS 7

To run the scripts
------------------

- open a terminal in the top level folder
- configure the target hosts in the inventory file 'hosts'
- run the following command 'ansible-playbook -i hosts basic_setup.yml'


